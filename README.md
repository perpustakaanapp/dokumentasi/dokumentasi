perpustakaan menggunakan kombinasi sql dan no sql yaitu 
1. customers dan borrows di mysql
2. books di mongo

books
- menggunakan fastapi dan mongo (local)
- menggunakan ODM (mongoengine)
- terdapat beberapa endpoint yang sudah ditest yaitu:
  a. /bookbyid
     untuk pencarian berdasarkan id
     	parameter : dictionary 
     	contoh : {"_id_":"607afe92f4276b3bb03a6e67"}
     	
      response : list books
          contoh : [
                    {
                        "_id": "607afe92f4276b3bb03a6e67",
                        "nama": "10-Day Green Smoothie Cleanse",
                        "pengarang": "JJ Smith",
                        "tahunterbit": 2016,
                        "genre": "Non Fiction"
                    },
                  ]
	
  b. /bookbyname
    untuk pencarian berdasarkan id
     	parameter : dictionary 
     	contoh : {"nama":"10-Day Green Smoothie Cleanse"}
     	
      response : list books
          contoh : [
                    {
                        "_id": "607afe92f4276b3bb03a6e67",
                        "nama": "10-Day Green Smoothie Cleanse",
                        "pengarang": "JJ Smith",
                        "tahunterbit": 2016,
                        "genre": "Non Fiction"
                    },
                  ]

  c. /books
    untuk pencarian semua buku
     	parameter :

      response : list books
          contoh : [
                    {
                        "_id": "607afe92f4276b3bb03a6e67",
                        "nama": "10-Day Green Smoothie Cleanse",
                        "pengarang": "JJ Smith",
                        "tahunterbit": 2016,
                        "genre": "Non Fiction"
                    },
                    {
                      "_id": "607afe92f4276b3bb03a6e68",
                      "nama": "11/22/63: A Novel",
                      "pengarang": "Stephen King",
                      "tahunterbit": 2011,
                      "genre": "Fiction"
                    }
                  ]

customer
- menggunakan flask dan mysql (cloud)
- menggunakan ORM (sqlalchemy)
- terdapat beberapa endpoint yang sudah ditest yaitu:
  a. users
    untuk pencarian semua customer
     	parameter :

      response : list customer
          contoh : [
                      {
                          "id": 1,
                          "username": "usersatu",
                          "namadepan": "Rudi",
                          "namabelakang": "roundhouse",
                          "email": "rudi.roundhouse@gmail.com"
                      },
                      {
                          "id": 3,
                          "username": "userdua",
                          "namadepan": "shiroe",
                          "namabelakang": "ishigami",
                          "email": "shiroe.ishigami@gmail.com"
                      },
                  ]

  b.user
      untuk pencarian berdasarkan id
     	parameter : dictionary 
     	contoh : {"userid": 1 }
     	
      response : list customer
          contoh : [
                      {
                          "id": 1,
                          "username": "usersatu",
                          "namadepan": "Rudi",
                          "namabelakang": "roundhouse",
                          "email": "rudi.roundhouse@gmail.com"
                      }
                  ]

  c.user/requesttoken
      untuk mendapatkan access token berdasarkan email
     	parameter : dictionary 
     	contoh : {"email": "rudi.roundhouse@gmail.com" }
     	
      response : list data token
          contoh : {
                      "data": {
                          "username": "usersatu",
                          "email": "rudi.roundhouse@gmail.com"
                      },
                      "token_access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6dHJ1ZSwiaWF0IjoxNjE4NzA3Njc3LCJqdGkiOiI0ZDg4M2FjYy1hZGE5LTQ0NWQtODJkMy0yZGZmNTI3Yjg5YWYiLCJuYmYiOjE2MTg3MDc2NzcsInR5cGUiOiJhY2Nlc3MiLCJzdWIiOnsidXNlcm5hbWUiOiJ1c2Vyc2F0dSIsImVtYWlsIjoicnVkaS5yb3VuZGhvdXNlQGdtYWlsLmNvbSJ9LCJleHAiOjE2MTg3OTQwNzd9.-Q0qnYq7dvUWu-StiYoI14A9FAo0H-14kH0m0gzeoi4"
                  }    


borrow
- menggunakan flask dan mysql (cloud)
- menggunakan ORM (sqlalchemy)
- terdapat beberapa endpoint yaitu:
  a. borrow
  b. borrow/insert
  c. borrow/update